package ru.psu.movs.trrp.tumblr;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;

public class App {
    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        Tumblr tumblr = new Tumblr();

        printPosts(tumblr.getPosts());

        tumblr.createPost(new Post().setTitle("Demo title " + java.util.UUID.randomUUID()).setBody("Demo body"));
        List<Post> posts = tumblr.getPosts();
        printPosts(posts);

        Post postToEdit = posts.get(ThreadLocalRandom.current().nextInt(posts.size()));
        postToEdit.setTitle("New title " + java.util.UUID.randomUUID());
        tumblr.editPost(postToEdit);
        printPosts(tumblr.getPosts());

        Post postToDelete = posts.get(ThreadLocalRandom.current().nextInt(posts.size()));
        tumblr.deletePost(postToDelete.getId());
        printPosts(tumblr.getPosts());
    }

    private static void printPosts(List<Post> posts) {
        for (Post post : posts)
            System.out.println(post.getId() + " " + post.getTitle() + " " + post.getBody());
    }
}
