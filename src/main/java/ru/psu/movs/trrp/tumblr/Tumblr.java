package ru.psu.movs.trrp.tumblr;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

class Tumblr {
    private final String urlPrefix = "http://api.tumblr.com/v2/blog/" + System.getenv("MOVS_TRRP_TUMBLR_BLOG_URL");

    private final RequestExecutor requestExecutor;

    Tumblr() throws InterruptedException, ExecutionException, URISyntaxException, IOException {
        requestExecutor = new RequestExecutor();
    }

    List<Post> getPosts() throws InterruptedException, ExecutionException, IOException {
        System.out.println(">> getPosts");
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.GET, urlPrefix + "/posts");
        Response response = requestExecutor.execute(oAuthRequest);
        JsonParser parser = new JsonParser();
        JsonArray postsJsonArray = parser.parse(response.getBody())
                .getAsJsonObject()
                .getAsJsonObject("response")
                .getAsJsonArray("posts");
        Gson gson = new Gson();
        return Arrays.asList(gson.fromJson(postsJsonArray, Post[].class));
    }

    void createPost(Post post) throws InterruptedException, ExecutionException, IOException {
        System.out.println(">> createPost");
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, urlPrefix + "/post");
        oAuthRequest.addBodyParameter("type", "text");
        oAuthRequest.addBodyParameter("title", post.getTitle());
        oAuthRequest.addBodyParameter("body", post.getBody());
        requestExecutor.execute(oAuthRequest);
    }

    void editPost(Post post) throws InterruptedException, ExecutionException, IOException {
        System.out.println(">> editPost " + post.getId());
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, urlPrefix + "/post/edit");
        oAuthRequest.addBodyParameter("id", String.valueOf(post.getId()));
        oAuthRequest.addBodyParameter("title", post.getTitle());
        oAuthRequest.addBodyParameter("body", post.getBody());
        requestExecutor.execute(oAuthRequest);
    }

    void deletePost(long id) throws InterruptedException, ExecutionException, IOException {
        System.out.println(">> deletePost " + id);
        OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, urlPrefix + "/post/delete");
        oAuthRequest.addBodyParameter("id", String.valueOf(id));
        requestExecutor.execute(oAuthRequest);
    }
}
