package ru.psu.movs.trrp.tumblr;

import com.github.scribejava.apis.TumblrApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.oauth.OAuth10aService;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

class RequestExecutor {
    private Future<OAuth1AccessToken> accessTokenF;
    private OAuth10aService service;

    RequestExecutor() throws IOException, ExecutionException, InterruptedException, URISyntaxException {
        CallbackServer cbs = new CallbackServer();
        cbs.start();
        service = new ServiceBuilder(System.getenv("MOVS_TRRP_TUMBLR_API_KEY"))
                .apiSecret(System.getenv("MOVS_TRRP_TUMBLR_API_SECRET"))
                .callback(cbs.getAuthUrl())
                .build(TumblrApi.instance());
        authenticate(cbs);
    }

    private void authenticate(CallbackServer cbs) throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        OAuth1RequestToken requestToken = service.getRequestToken();
        accessTokenF = cbs.getOAuth1VerifierCF().thenApply(oauthVerifier -> {
            try {
                return service.getAccessToken(requestToken, oauthVerifier);
            } catch (Exception e) {
                throw new CompletionException(e);
            }
        });
        String authUrl = service.getAuthorizationUrl(requestToken);
        Desktop.getDesktop().browse(new URI(authUrl));
    }

    Response execute(OAuthRequest request) throws ExecutionException, InterruptedException, IOException {
        service.signRequest(accessTokenF.get(), request);
        return service.execute(request);
    }
}
